package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeState;
import com.conygre.training.tradesimulator.model.TradeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    private static RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());;

    @Transactional
    public List<Trade> findTradesForProcessing(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.CREATED);

        for(Trade thisTrade: foundTrades) {
            thisTrade.setState(TradeState.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.PROCESSING);
        //template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        for(Trade thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 8) {
                thisTrade.setState(TradeState.REJECTED);
                tradeDao.save(thisTrade);
            }
            else {
                thisTrade.setState(TradeState.FILLED);
                tradeDao.save(thisTrade);
                switch(thisTrade.getType()){
                    case BUY:
                        template.postForObject("http://localhost:8080/api/trades/stock/buy", thisTrade, Trade.class);
                        break;
                    case SELL:
                        template.postForObject("http://localhost:8080/api/trades/stock/sell", thisTrade, Trade.class);
                        break;
                    default:
                        break;
                }
                
            }
            
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
